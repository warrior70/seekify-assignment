package seekify.linkedin.login.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import seekify.linkedin.login.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
	Optional<User> findByLinkedInId(String linkedInId);

    Boolean existsByLinkedInId(String linkedInId);
}