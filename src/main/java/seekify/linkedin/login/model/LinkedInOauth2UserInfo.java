package seekify.linkedin.login.model;

import java.util.Map;

public class LinkedInOauth2UserInfo extends OAuth2UserInfo {
	
    public LinkedInOauth2UserInfo(Map<String, Object> attributes) {
        super(attributes);
    }

    @Override
    public String getId() {
        return (String) attributes.get("id");
    }

    @Override
    public String getName() {
        return (String) attributes.get("localizedFirstName") + " " +
        		(String) attributes.get("localizedLastName");
    }
}
