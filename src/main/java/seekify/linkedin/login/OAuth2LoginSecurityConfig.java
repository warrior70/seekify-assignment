package seekify.linkedin.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import seekify.linkedin.login.service.OAuth2UserService;

@EnableWebSecurity()
public class OAuth2LoginSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private OAuth2UserService customOAuth2UserService;

	private OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> customAccessTokenResponseClient() {
		return new OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest>() {
			
			@Override
			public OAuth2AccessTokenResponse getTokenResponse(OAuth2AuthorizationCodeGrantRequest authorizationGrantRequest) {
				ClientRegistration client = authorizationGrantRequest.getClientRegistration();
				HttpResponse<JsonNode> response = Unirest.get(client.getProviderDetails().getTokenUri())
					.queryString("client_id", client.getClientId())
					.queryString("client_secret", client.getClientSecret())
					.queryString("grant_type", "authorization_code")
					.queryString("redirect_uri", authorizationGrantRequest.getAuthorizationExchange().getAuthorizationRequest().getRedirectUri())
					.queryString("code", authorizationGrantRequest.getAuthorizationExchange().getAuthorizationResponse().getCode())
					.asJson();
				String accessToken = response.getBody().getObject().getString("access_token");
				long expiresIn = response.getBody().getObject().getLong("expires_in");
				
				System.out.println(response.getBody());
				OAuth2AccessTokenResponse res = OAuth2AccessTokenResponse
						.withToken(accessToken)
						.tokenType(OAuth2AccessToken.TokenType.BEARER)
						.expiresIn(expiresIn).build();
				return res;
			}
		};
    }
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
        	.authorizeRequests()
        		.antMatchers("/login/oauth2/code/**", "/login.html")
        			.permitAll()
                .anyRequest()
                	.authenticated()
                .and()
            .oauth2Login()
            	.loginPage("/login.html")
            	.authorizationEndpoint()
                	.baseUri("/oauth2/authorization")
                	.and()
            	.tokenEndpoint()
            		.accessTokenResponseClient(customAccessTokenResponseClient())
            		.and()
            	.userInfoEndpoint()
                    .userService(customOAuth2UserService);
    }
}