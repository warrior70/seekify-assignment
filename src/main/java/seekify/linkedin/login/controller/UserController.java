package seekify.linkedin.login.controller;

import org.springframework.web.bind.annotation.RestController;

import seekify.linkedin.login.model.User;
import seekify.linkedin.login.model.UserPrincipal;
import seekify.linkedin.login.repository.UserRepository;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping("/me")
	public User user(Principal principal) {
		OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) principal;
		UserPrincipal userPrincipal = (UserPrincipal) token.getPrincipal();
		return userRepository.findByLinkedInId(userPrincipal.getId()).get();
	}
}